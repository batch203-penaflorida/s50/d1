import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react";

import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Courses from "./pages/Courses";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";
import CourseView from "./pages/CourseView";

import "./App.css";
import { UserProvider } from "./UserContext";
/* 
  All other components/pages will be contained in our main (parent) component : App
*/

/* 
  We use react fragments <></>
*/

// Create a "User" state and an "unsetUser" function that will be used in different pages/components within the application.

// Global state hooks for the user information for validating if a user is logged in.

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);
  // To update the User state upon a page load is intiated and if a user already exists.
  useEffect(() => {
    fetch("https://whispering-anchorage-44652.herokuapp.com/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // Change the global "user" state to store the "id" and is "isAdmin"
        // Set the user states value if the token already exists in the local storage
        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        }
        // set back to the intial state of the user if no token found in the local storage.
        else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);
  return (
    // Router component is used to wrap around all components which will have access to the routing system.
    // All information
    // We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value prop."
    // All the information inside the value prop will be accessible to
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          {/* Routes hold our Route components. */}
          <Routes>
            {/* 
            localhost:3000/user/login 
            localhost:3000/user/profile
          */}
            {/* 
          Assign an endpoint and display appropraite page component for that endpoint
              - "exact" and path props to assign the endpoint and the page should be only accessed on the specific endpoint.
              - "element" props assigns page components to the displayed endpoint
              . 
         */}
            {user.id !== null ? (
              <>
                <Route exact path="/logout" element={<Logout />} />
                <Route exact path="/login" element={<Courses />} />
                <Route exact path="/register" element={<Courses />} />
              </>
            ) : (
              <>
                <Route exact path="/logout" element={<Courses />} />
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/register" element={<Register />} />
              </>
            )}
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/courses/:courseId" element={<CourseView />} />
            <Route exact path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
