import { useEffect, useState } from "react";
// import {
//   validEmail,
//   validName,
//   validMobileNo,
//   validPassword,
// } from "../regex/Regex";
import { validMobileNo, validEmail, validPassword } from "../regex/Regex.js";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { Button, Form } from "react-bootstrap";

export default function Register() {
  // Create state hooks to store the values of the input fields.
  const navigate = useNavigate();
  const [fName, setFname] = useState("");
  const [lName, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState("");
  const [cpassword, setCpassword] = useState("");

  // console.log(`First Name: ${fName}`);
  // console.log(`Last: ${lName}`);
  // console.log(`Email: ${email}`);
  // console.log(`Mobile Number: ${mobileNo}`);
  // console.log(`Password: $Y{password}`);
  // console.log(`Confirm Password: ${cpassword}`);

  // create a state to determine whether the submit button is enabled or not.
  const [isActive, setIsActive] = useState(false);
  useEffect(() => {
    // if (fName.length < 3) {
    //   console.log("Invalid");
    // }
    // if (lName.length < 3) {
    //   console.log("Invalid");
    // }
    // if (!validEmail.test(email)) {
    //   console.log("Invalid");
    // } else {
    //   console.log("Valid");
    // }
    // if (!validMobileNo.test(mobileNo)) {
    //   console.log("Invalid");
    // } else {
    //   console.log("Valid");
    // }
    // if (!validPassword.test(password) && !validPassword.test(cpassword)) {
    //   console.log("Invalid");
    // } else {
    //   console.log("Valid");
    // }
    if (
      fName !== "" &&
      lName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      password !== "" &&
      cpassword !== "" &&
      password === cpassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    // Enable the submit button if:
    // All the fields are populated
    // both passwords match
  }, [fName, lName, email, mobileNo, password, cpassword]);
  /* 
    Two way embedding
      - Is done so that we can assure that we can save the input into our states as we type into the input elements. This is so we don't have to save it just before submit.

    
  */
  // Create a satte to determine whether the submit button is enable or not.

  /* Registration form fields
      -firstName
      -lastName
      -email
      -mobileNumber
      -password
      -confirm password
   */
  //  Function to simulate user registration
  function registerUser(e) {
    // prevents page loading / redirection via form submission.
    e.preventDefault();
    // Clear input fields
    // alert(`Thank you for registering ${fName}`);
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Kindly provide another email to complete the registration.",
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: fName,
              lastName: lName,
              email: email,
              mobileNumber: mobileNo,
              password: password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to Zuitt!",
                });
                setFname("");
                setLname("");
                setEmail("");
                setMobileNo("");
                setPassword("");
                setCpassword("");
                // Allow us to redirect the user to the login page after account registration

                navigate("/login");
              } else {
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again.",
                });
              }
            });
        }
      });
    // Notify user for registration
  }
  return (
    <>
      <h1 className="my-5 text-center">Register</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group className="mb-3" controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            value={fName}
            onChange={(e) => setFname(e.target.value)}
            placeholder="Enter first name"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            value={lName}
            onChange={(e) => setLname(e.target.value)}
            placeholder="Enter last name"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter email"
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="mobileNo">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
            type="tel"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            placeholder="09#########"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Enter password"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="cpassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            type="password"
            value={cpassword}
            onChange={(e) => setCpassword(e.target.value)}
            placeholder="Confirm password"
            required
          />
        </Form.Group>

        {/* Conditional rendering - submit button will be active based on the isActive state */}

        {isActive ? (
          <Button
            variant="primary"
            className="my-3"
            type="submit"
            id="submitBtn"
          >
            Submit
          </Button>
        ) : (
          <Button
            variant="danger"
            className="my-3"
            type="submit"
            id="submitBtn"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
