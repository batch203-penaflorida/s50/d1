import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
// import coursesData from "../data/coursesData";

// course state tht will be used to store courses retrieve in the database.

export default function Course() {
  const [courses, setCourses] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  // The curly braces are ({}) are used for props to signify that we are providing information using a JavaSCript expressions.

  // Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesData array using courseProp.
  // Multiple components created through the map method must have a unique key that will have multiple components
  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });
  return (
    <>
      <h1>Courses</h1>
      {courses}
    </>
  );
}
