import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {
  //   Redirect back to login
  // Allows us to clear the information in the localStorage
  const { unsetUser, setUser } = useContext(UserContext);
  unsetUser();
  useEffect(() => {
    setUser({ id: null, isAdmin: null, name: null });
  });
  return (
    <>
      <Navigate to="/login" />
    </>
  );
}
