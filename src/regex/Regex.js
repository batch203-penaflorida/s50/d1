export const validEmail = new RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");

export const validMobileNo = new RegExp("^([0|+[0-9]{1,5})?([7-9][0-9]{9})$");

export const validPassword = new RegExp("^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$");
