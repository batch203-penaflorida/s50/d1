import React from "react";
import ReactDOM from "react-dom/client";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/* 
  root.render() - allows us to render/display our reactjs elements and show it in our HTML document.

*/
/* const name = "John Smith";
// const element = <h1>Hello, {name} </h1>;
const user = {
  firstName: "Jane",
  lastName: "Smith",
};
function formatName(fullName) {
  return `${fullName.firstName} ${fullName.lastName}`;
} */

/* 
  The h1 tag is an example of what call JSX. 
  It allows us to create HTML elements and at the same time allows us to apply JavaScript code to our elements.
  JSX makes it easier to write both HTML and JavaScript code in single file as opposed to making separate files. 
*/
/* const element = <h1>Hello, {formatName(user)}</h1>;
root.render(element);
 */

/* 
  React import pattern:
    - imports from build-in react modules.
    - imports from downloaded packages.
    - imports from user defined components.
*/
