// import Container from "react-bootstrap/Container";
// import Nav from "react-bootstrap/Nav";
// import Navbar from "react-bootstrap/Navbar";
// import NavDropdown from "react-bootstrap/NavDropdown";
// shorthand method
import { Container, Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useState, useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar() {
  // Create user state that will be used for the conditional rendering of our navbar.
  /*
     - "as" prop allows components to be treated as the component of the "react-router-dom" to gain access to it's properties and functionalities.
     - "to" prop is used to in place of the "href" attribute for providing the URL for the page.
  */
  /* 
    Use the user global state that will be used for the conditional rendering of our nav bar.
  */
  const { user } = useContext(UserContext);
  console.log(user);
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/" end>
          Zuitt
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {/* className is use instead of class to specifiy a CSS class. 
            ml - ms (margin start)
            mr - me (margin end)

            If user is login, nav links visible:
              -Home 
              -Courses
              -Logout

            If user is not login, nav links visible:
              -Home
              -Courses
              -Login
              -Register
          */}
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" end>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/courses" end>
              Courses
            </Nav.Link>

            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout" end>
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" end>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" end>
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
