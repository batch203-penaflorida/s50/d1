import { useState, useEffect } from "react";
import { Card, Col, Row, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  // console.log(courseProp);
  // Scenario: Keep track the number of enrollees of each course.

  // Destructuring
  const { _id, name, description, price, slots } = courseProp;
  /* Syntax
    const [stateName, setStateName] = useState(initialStateValue);
      Using the state hook, it returns an array with the following elements.
        // first element contains the  current initial state value.
        // second element is a function that is used to change the value of the first element value.
  */
  // Use the disabled state to disable the enroll button.
  // const [count, setCount] = useState(0);
  // const [seat, setSeat] = useState(30);
  // const [disabled, setDisabled] = useState(false);
  // Syntax:
  // useEffect(function, [dependencyArr])
  // No dependency array passed
  // If the useEffect() does not have a dependency array, it will run on initial render and whenever a state is set but by its function.
  /*
    // With dependency array
  // useEffect(() => {
  //   console.log(
  //     "Will run on initial render or on every changes with our components"
  //   );
  // });
  useEffect(()=>{
  console.log("Will run on intial render or on every changes within our components.")
  }) 
  */
  // An empty array
  // If the useEffect() has dependency array but it is empty, it will only run on the initial render.
  // useEffect(() => {
  //   console.log("Will only run on initial render.");
  // }, []);

  /*With dependency array (props or state values)  */

  // if the useEffect() has a dependency array and there is state or data in it, the useEffect will run whenever that state is updated
  // useEffect(() => {
  //   console.log(
  //     "Will run intitial render and every change on the dependency value."
  //   );
  // }, [seat]);
  /* 
    We will refactor the "enroll" function using the useEffect hooks to disable the enroll button when the seats reach zero.
  */

  // function enroll() {
  //   // 0 + 1;
  //   // if (seat > 0) {
  //   setCount(count + 1);
  //   setSeat(seat - 1);
  //   console.log(`Enrollees: ${count}`);
  //   console.log(`Seats: ${seat}`);
  //   // } else {
  //   //   alert("No more seats available.");
  //   //   setDisabled(true);
  //   // }
  // }

  // useEffect(() => {
  //   if (seat <= 0) {
  //     setDisabled(true);
  //     alert("No more seats avaialble.");
  //   }
  // }, [seat]);
  // console.log(useState(10));
  // onClick={fucntion}
  return (
    <Row>
      <Col md={12} className="my-3">
        <Card>
          <Card.Body>
            <Card.Title className="mb-3">{name}</Card.Title>
            <Card.Subtitle className="mb-2">Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle className="mb-2 ">Price</Card.Subtitle>
            <Card.Text>₱ {price}</Card.Text>
            <Card.Subtitle>Slots:</Card.Subtitle>
            <Card.Text>{slots} available</Card.Text>

            <Button as={Link} to={`/courses/${_id}`} variant="primary">
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
