/* 
   [SECTION] Creating React Application: 
      Syntax:
         npm create-react-app <project name>

   Delete uncessary files from the app
      application > src

   App.test.js
   index.css
   logo.svg
   reportWebVitals.js
   
   JSX- JavaScript + XML, It is an extension of JavaScript that let's us create objects which will then be compiled and added as HTML elements.

   - With JSX, we are able to create HTML elements using JS.

   - With JSX, we are able to create JS objects that will then be compiled and added as HTML elements.
*/

/* 
   React JS Component
      - This are reusable parts of our react application.
      - They are independent UI parts of our application.
      - Components are functions that react return react elements.
      - Components naming convention: PascalCase 

         Capitalized letter for all words of the function name AND file name associated with it.
      
      
      
      
*/

// React Bootstrap
/* 
   Syntax:
      import {moduleName/s} from "file path"
   
*/

/* 
   Props passing property in react js

   Props:
      - is a shorthand for "property" since components are considered as object in React JS.
      - is a way to pass a data form a parent component to a child component.
      - it is synonymous to function parameters.
      - it is used like an html attribute added to the child component.

   Manage data within React JS Component

   Pass data from between React JS 

   Props in React.js, short for properties, Props are information that a component receives, usually from a parent component.

   Props are synonymous to the function

   What is it for?

   props are reusable

   One can use the same component and feed different data to the component for rendering.

   States in React.js allow components to create manage its own data and is meant to be used internally.

   Hooks in React.js are functions that allow developers to create and manage states and lifecycle within components For states, there is function called useState()

*/
/* 
   States 
   - States are a way to store information within component. This information can then be updated within the component.
   - States are used to keep track of information related to individual components.
*/

/* 
   Hooks
   useState
   useEffect
   useContext
  

   - Special/React-defined methods and functions that allows us to do certain tasks in our components.
   - Use the state hook for this componenet to be able to store its state.
*/
/* 
React.js - Effects, Events and Forms
*/

/* 
   Apply an effect hook to handle events and validate 
*/
/* 
   Execute a piece of code whenever a component gets rendered to the page or if the value of a state changes
   To ensure that the value of the input is the same - Two Way data binding


   Effect Hooks in react allows us to execute a piece of code whenever a components gets renderd to the page or if the  value of a state changes.

   This is especially useful if we want our page to be reactive.
   Examples of side effects (simply effects) are fetching data from the server, or changing the contents of a page.

   Handling events with React elements is very similar to handling event on DOM Elements. There are some syntax difference thoough

   React events are named using camelCase, rather than lowercase.
   With JSX you pass a function as the event handler, rather than a string.

   Then, In React:

   Nasa isang file



*/
/* 
   React.js - Effects, Events and Forms
   Effect hooks in React allow us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes.
   
   useEffect() allows us to perform a "side effects" in your components or run a specific task.
      // Some examples of side effects are: fetching data, directly updating the DOM, and timers.

      // Syntax:
         // useEffect(function, [dependency])

      useEffect() always runs the task on the initial render and or every render (when changes in a component).
         // Initial render is when the component is run or displayed for the first time.


*/

// Routing and Conditional Rendering
//Using "Browser router", it will allow us to simulate changing pages in react, Because by default, react is used for SPA(Single Page Application).

// Buttons -> <Link> to navigate to other pages
// Navbar -> <NavLink> to navigate to other pages and set the active NavLink.

// Conditional Rendering
// It allows us to show a components based only when a given condition is met. (e.g. Show Submit button if all the fields are populated.)

/* 
   App State Management

      Manage and pass data within a ReactJS application.

      Manage and pass data between components within an app.

      So far we have learned how to manage state within individual components. However, it sometimes required to handle the state of the whole application.

      Context provides a way to share values like these between components without having to explicitely pass a prop to the
*/

/* State Hooks a way to store information within a component and track this information.

Effect Hooks (useEffect) - allows us to execute a piece of code whenever a component gets rendered or perform a "side effect"

React Context API (useContext) - provides a way to share values (state) between components without having to explicitly pass a prop through each component.
   - Share values (state) between components without having to explicitly pass a "prop" through each component.

*/

/* 
   React Context
      allows us to pass down and use (consume) data in any component we need in our React Application without using "props".

      In other words, React context allows us to share data (state, function, etc.) accross component more easily.

      // 3 simple steps in using React Context
         1. Creating the context.
         2. Providing the context.
         3. Consuming the context.
*/
